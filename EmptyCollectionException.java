/**
 * emptyCollectionException
 * Provides a nice message when we try to remove from an empty heap.
 */
public class EmptyCollectionException extends RuntimeException {
    public EmptyCollectionException() {
        System.err.println("Operation not allowed; the collection is empty.");
    }
}