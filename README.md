Search Heap
===========

This is a data structure I am in the process of writing. As evidenced by the name, this structure is a well-ordered tree, thus it is also searchable. It is a type of heap that keeps both the global minimum and maximum in the root node, and also keeps the local minima and maxima in their respective subtree's root. This ensures *O*(1) search time for the extrema, and, because it is self-balancing, it guarantees *O*(*lg n*) search time for a given element, where *n* is the number of elements in the collection.

A zero-padded example with integers:

                 (00, 13)
                 /      \
        (01, 06)          (07, 12)
           / \               / \
    (02, 03) (04, 05) (08, 09) (10, 11)
