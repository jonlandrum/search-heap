/**
 * UnknownErrorException
 * Provides a nice message when something wonky
 * happens that we cannot explain.
 * 
 * Written by Jonathan Landrum
 */
public class UnknownErrorException extends RuntimeException {
    public UnknownErrorException() {
        super("An error has occurred.");
    }
}