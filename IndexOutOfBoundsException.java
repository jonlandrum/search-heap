/**
 * IndexOutOfBoundsException
 * Provides a nice message when we try to pick a number
 * that is outside of the number of elements in the heap.
 * 
 * Written by Jonathan Landrum
 */
public class IndexOutOfBoundsException extends RuntimeException {
    public IndexOutOfBoundsException(int n) {
        String bounds = n < 1 ?  "less than 1." :
            "greater than the number of elements in the collection.";
        System.err.println("Invalid input. The number entered is " + bounds);
    }
}