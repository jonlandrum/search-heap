/**
 * NoSuchElementException
 * Provides a nice message when the element selected does not exist.
 * 
 * Written by Jonathan Landrum
 */
public class NoSuchElementException extends RuntimeException {
    public NoSuchElementException() {
        System.err.println("The element requested does not exist.");
    }
}